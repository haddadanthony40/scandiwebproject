<?php
require('Database.php');
?>
<?php
$c="SELECT COUNT(*) as count From Items";
$result=mysqli_query($conn, $c) or die(mysqli_error($conn));
$count=mysqli_fetch_assoc($result);
if (isset($_POST['SKU'])) {
        $SKU = mysqli_real_escape_string($conn,$_POST["SKU"]);
		$name = (mysqli_real_escape_string($conn,$_POST["name"]));
		$price = mysqli_real_escape_string($conn,$_POST["price"]);
		$type = 	(mysqli_real_escape_string($conn,$_POST["productType"]));
		if($type=='DVD'){
		$attribute =	(mysqli_real_escape_string($conn,$_POST["size"]))."MB";
		}
		else if($type=='Book'){
		    $attribute =	(mysqli_real_escape_string($conn,$_POST["attribute"]))."KG";
		}
		else{
		    $attribute =	(mysqli_real_escape_string($conn,$_POST["height"]))."x".(mysqli_real_escape_string($conn,$_POST["width"]))."x".(mysqli_real_escape_string($conn,$_POST["length"]));
		}
		$id = $count['count'] +1;
		$q = "INSERT INTO Items (SKU,name,price,attribute,ID,type) VALUES ('$SKU','$name','$price','$attribute','$id','$type')";
		mysqli_query($conn, $q) or die(mysqli_error($conn));
}
?>
<?php
echo "
<head>
<style>
body{
    background-image:url('white-background-design-free-vector.jpg');
}
@font-face {  
 font-family: mySecondFont;
    src: url(LEMONMILK-Bold.otf);
}
*{
    font-family:mySecondFont;
    font-size:24px;
}
.button {
  background-color: white;
  border-color: black;
  color: black;
  text-align: center;
  font-size: 16px;
  height:5%;
  width:7%;
}
.button:hover{
    background-color: black;
  border: white;
  color: white;
}
</style>
<script>
function myFunction() {
    var x = document.getElementById('productType').value;
    if(x == 'DVD'){
       document.getElementById('attribute2').innerHTML = `Size<input id='size' name='size' type='number' placeholder='Size In MB...' style='margin-left:5%' required>`;
    }
    else if(x == 'Book'){
       document.getElementById('attribute2').innerHTML = `Weight<input id='weight' type='number' name='attribute' placeholder='Weight In KG...' style='margin-left:2%' required>`;
    }
    else if(x == 'Furniture'){
       document.getElementById('attribute2').innerHTML = `Height<input id='height' name='height' type='number' placeholder='Height In CM...' style='margin-left:2%' required>
       <div style='margin-top:2%'>
       Width<input type='number' id='width' name='width' placeholder='Width In CM...' style='margin-left:3%' required>
       </div>
       <div style='margin-top:2%'>
       Length<input type='number' id='length' name='length' placeholder='Length In CM...' style='margin-left:2%' required>
       </div>
       `;
    }
}
</script>
</head>
<form id='product_form' action='".$_SERVER['PHP_SELF']."' method='POST'>
<body>
<h1 style='font-size:50px'>Add Product
<div style='display:inline;margin-left:40%'>
<input type='submit' name='savebtn' class='button' value='Save'>
<a href='index.php'><button type='button' class='button' name='cancelbtn' style='margin-left:5%'>Cancel</button></a>
</div>
<hr>
</h1>
<div>
SKU<input type='text' id='sku' name='SKU' placeholder='Enter SKU Here...' style='margin-left:5%' required>
</div>
<div style='margin-top:2%'>
Name<input type='text' id='name' name='name' placeholder='Enter Name Here...' style='margin-left:3%' required>
</div>
<div style='margin-top:2%'>
Price<input type='number' id='price' name='price' placeholder='Enter Price Here...' style='margin-left:43px' required>
</div>
<div style='margin-top:2%'>
Type<select id='productType' name='productType' style='margin-left:4%' onchange='myFunction()' required>
<option value='' disabled hidden selected>Pick An Option</option>
<option value='DVD'>DVD</option>
  <option value='Book'>Book</option>
  <option value='Furniture'>Furniture</option>
  </select>
</div>
<p id='attribute2'>
</p>
</form>
";
?>
